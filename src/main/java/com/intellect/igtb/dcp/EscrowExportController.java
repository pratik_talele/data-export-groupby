package com.intellect.igtb.dcp;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.ICBXBackendResponse;
import com.intellect.igtb.dcp.export.controller.ExportServiceController;
import com.intellect.igtb.dcp.service.PrincipalExport;

@RestController
@RequestMapping("/export")
public class EscrowExportController {

	@Autowired
	PrincipalExport principalExport;

	@RequestMapping(value = "/principal", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> principal(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		System.out.println("in principal");
		return new ExportServiceController(principalExport).execute(payload, headers);
	}

}