package com.intellect.igtb.dcp.utilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellect.igtb.dcp.caf.gqlactions.models.Context;
import com.intellect.igtb.dcp.cloudbocommons.connector.models.ResponsePayLoad;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.GQLResponse;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult.val_status;
import com.intellect.igtb.dcp.cloudbocommons.utils.text.MutationUtils;
import com.intellect.igtb.dcp.cloudbocommons.utils.validation.JsonPathUtils;

import net.minidev.json.JSONArray;

@SuppressWarnings("unchecked")
@Service
public class Utilities {
	private static final Logger logger = LoggerFactory.getLogger(Utilities.class);

	private static final String VA_SUMMARY_LIST = "/hasuraormconfig/principal.gql";

	
	public Map<String, Object> getVASummaryList(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getPrincipalData");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

		//change start
		
		String groupCriteria = (String) payload.get("groupCriteria");
		if (StringUtils.isNotEmpty(groupCriteria) && StringUtils.isNotBlank(groupCriteria)) {
			if (groupCriteria.equals("principal")) {
				argumentMap.put("gbprincipal", true);
			}
		} else {
			argumentMap.put("gbphys_acc", true);
		}
		
		//change end
//		argumentMap.put("gbphys_acc", true);
		HashMap<String, Object> searchOptionMap = new HashMap<>();
		String searchText = (String) payload.get("searchText");
		if (StringUtils.isNotEmpty(searchText) && StringUtils.isNotBlank(searchText)) {
			searchOptionMap.put("accountOwnerType", Collections.singletonMap("_eq", searchText));
		}
//		change in inpmap for checking commans
//		inpMap.put("argument", argumentMap);
//		inpMap.put("searchOption", searchOptionMap);
		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_SUMMARY_LIST, inpMap);

		logger.info("Reponse Payload{}", responsePayload);
	
		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master_work");
		logger.info("get all principal accounts"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master_work"));
		
//		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
//				"$.data.escrow_va_account_master");
//		logger.info("get all principal accounts"
//				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

		va_summary.put("vam_summary", jsonArray);
		logger.debug("OUT - getVASummaryList {}", va_summary.toString());
		return va_summary;
	}

	
	public String convertModelIntoJson(final Object request) {
	
		final ObjectMapper mapper = new ObjectMapper();
		final Map<String, Object> map = mapper.convertValue(request, Map.class);
		final StringBuffer jsonBuffer = new StringBuffer();
		MutationUtils.convertHashMapToMutationJSON(map, jsonBuffer);
		return jsonBuffer.toString();
	}

	
	public String getQueryResponse(final Context context, final String lookupTemplate,
			final HashMap<String, Object> queryVariables) {
		String lookUpQry = context.getHasuraConfig().getMutationTemplate(lookupTemplate);
		GQLResponse gqlResponse = null;
		try {
			// Fire the query.
			ResponsePayLoad responsePayload = context.getHttpConnector().executeGQLRequest(lookUpQry, queryVariables,
					context.getHeaders());
			if (!responsePayload.isValid())
				throw new CBXException(responsePayload.getErrorCode(), responsePayload.getErrorMessage());
			// Parse the query output , checking in product
			gqlResponse = new GQLResponse(responsePayload.getPayload());
		} catch (CBXException ex) {
			logger.debug("Exception {}", ex.getStackTrace().toString());
			ex.printStackTrace();
		}
		return gqlResponse.getResponsePayload();
	}

	
	public void addValidationResult(final FieldValidationResult result,
			final Map<String, FieldValidationResult> valResponseMap) {
		if (result.getStatus() == val_status.FAIL) {
			valResponseMap.put(result.getErrorCode(), result);
		}
	}

}
