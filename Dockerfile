FROM openjdk:8-jdk-alpine
RUN addgroup -S cbxadmin && adduser -S cbxadmin -G cbxadmin
USER cbxadmin:cbxadmin
ARG JAR_FILE=target/*exec.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]